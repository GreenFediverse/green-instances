**What is important when researching?**

A community in the Fediverse (be it Mastodon, Peertube, etc.) has its home on a server somewhere on the Internet. Our script checks all these servers and shows us if they have been rated green by [The Green Web Foundation](https://www.thegreenwebfoundation.org/).

To enhance this with more information about the server, we include
an open research process with following goals:

1. Make greenwashing transparent
2. Clarifying dependencies
3. More information on the energy powering the servers

What should be considered:

**1. Greenwashing is very present**

Information provided by the hosting provider of the server himself should be viewed critically.

Making ethical corporate responsibility visible is itself ethical action. Statements by companies on this are thus an expression of possible ethical action. But unfortunately, this often only takes place out of motivation, if this leads to the improvement of their own image and the creation of profit. When the creation of transparency is not done out of honest conviction, this must be viewed critically, so that third parties, i.e. others need to create visibility and understanding for this actions.
It is also suggested that self-imposed responsibility would be sufficient, but this is often only intended to prevent the creation of new laws so that companies do not come under pressure and can continue to make profits undisturbed. They are creating a trade in indulgences instead of real transformation. Here, the creation of transparency of a social-ecological corporate action is important. Companies often have an interest in hiding unwelcome information like their use of fossil fuels. Always use third-party information if available.

However, some companies are not only focused on renewable energy. Companies often use a spin-off for this, or establish a subsidiary, for example, to cover up this practice mostly for image and marketing reasons. Open databases such as [OpenCorporates](https://opencorporates.com/) lists companies in an open database, and in the best case can show us what a subsidiary belongs to. They give themselves a green image, but at the same time continue to invest in dirty energy.

To be clear, preventing CO2 in the first place must be by far our first priority. We have to move to a world, where the servers, the cooling of the servers, the production of the server, the workbuildings, and everything else electrically is powered by wind and solar. But of course, not every emitted CO2 is caused by electricity, so it is also good when hosting providers measure their CO2 footprint and provide compensation and a plan to lower their footprint in the future. Here are some examples on how the CO2 footprint can be lowered: use of refurbished hardware, use of already exisiting buildings for datacenters (e.g. [ungleich.ch](https://ungleich.ch/en-us/cms/blog/2019/06/28/how-run-really-green-datacenter/)), use vendors known for good sustainability (e.g. [hostsharing.net](https://www.hostsharing.net/vorteile/nachhaltigkeit-und-soziale-gemeinschaft/)), use good materials when building a new datacenter, giving employees benefits when they come by bike to work, ...

**2. Server hosting is complex**

To host a server you need: computers/servers, some place to put them (data center), electricity to power and cool them,
good internet connection and everything doubled on a different place (backup). For a complete picture, it is recommended to also check the companies e.g. behind the internet service provider (ISP). Often, hosting providers rent a room in a datacenter by so called „colocation providers“. Which means that they consume the internet and the electricity of the datacenter and rely on their sustainability strategy.

Sometimes it helps to look for the geographical location of the server by entering its internet-address in a "whois" service (for example: [whois.domaintools.com](https://whois.domaintools.com/))

**3. Energy - at the end everything is about energy and where it comes from.**

Often, data center providers have a nicely prepared website where we can get information on the use of renewable energy. Often it is enough if we enter terms such as "renewable energy", "environment" or "sustainability" on the respective page of the hosting provider. But also in a search engine of your choice, often the name of the hosting provider in connection with these terms is enough. In the best case, we will receive a link to an energy certificate, which has been uploaded and made accessible as a .pdf. Here we then get the name of the power provider and whether it is powered with renewable energy.

Next step is to continue with the research on the energy provider itself. Often, those providers are clarified as green but make no investment in an energy transition from nuclear and fossil-fuel energy to wind and solar. Also hydroelectric has to be seen critical sometimes, e.g. if a new plant is built in a nature reserver.

Positive examples are e.g. [Green-Planet-Energy](https://green-planet-energy.de/privatkunden.html) and [Naturstrom AG](https://www.naturstrom.de/).
Other good energy providers can be found e.g. here: [https://utopia.de/bestenlisten/die-besten-oekostrom-anbieter/#kriterien](https://utopia.de/bestenlisten/die-besten-oekostrom-anbieter/#kriterien) and [https://greenecofriend.co.uk/eco-friendly-energy-suppliers/](https://greenecofriend.co.uk/eco-friendly-energy-suppliers/)

A must-read to understand the negative examples and how they do greenwashing: [https://www.thegreenwebfoundation.org/what-you-need-to-register/](https://www.thegreenwebfoundation.org/what-you-need-to-register/)

Often it makes sense to check which companies are ultimately receiving the money at the end. Here's an example (which
is not that worse but can show the relations): Biohost gets the energy for their servers from the energy provider Polarstern, which gets the energy from Verbund AG power plants, which in turn run hydroelectric plants. And now let's change perspective: Biohost only pays for green electricity from a chosen Polarstern tariff, Polarstern buys the energy from Verbund AG, Verbund AG uses the profits from it to buy and sell harmful energy on energy-markets, or build new controversial power-plants. So we moved on with Verbund AG and continued to ask critical questions about what the company's real energy mix looks like. We found out by entering the term: [Stromkennzeichnung](https://de.wikipedia.org/wiki/Stromkennzeichnung) on their website. In countries such as Germany or Austria, companies are required to disclose the composition of their energy mix once a year "on the annual electricity bill and on all promotional materials", also the Dealer and Supplier mix. These are then usually published in annual reports as .pdf on their websites. In Austria E-Control has been monitoring the implementation of electricity labelling by electricity suppliers in its capacity as supervisory authority since 2005. Here "every electricity supplier, that delivers electricity to final customers in Austria, has to announce the energy mix of his supply. Also information about the CO2 emissions and radioactive waste must be published. [here](https://www.e-control.at/en/publikationen/oeko-energie-und-energie-effizienz/berichte/stromkennzeichnungsbericht?inheritRedirect=true) you will find these annual reports. In Great Britain it’s called "[Fuel Mix Disclosure](https://www.ofgem.gov.uk/environmental-and-social-schemes/renewables-energy-guarantees-origin-rego/energy-suppliers/fuel-mix-disclosure-fmd)".

To get more information on the hosting provider and their energy suppliers, sometimes you have to contact them directly.
