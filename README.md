# Welcome to GreenFediverse!

<img src="https://greenfediverse.codeberg.page/green-instances/greenfediverse_logo.png" class="center" width="150" height="150" alt="GreenFediverse">

Why not being the first decentralized social network that runs on renewable energy? Sounds utopian, but we do it anyway!💚

> GreenFediverse is an open project to empower the Fediverse to run on servers with renewable energy and creating awareness for a sustainable development of it.

This project provides a critical list about servers and their renewable energy consumption status in a **[Green Instances List](https://greenfediverse.codeberg.page/green-instances/)**. A community in the [Fediverse](https://en.wikipedia.org/wiki/Fediverse) (be it a [Mastodon](https://joinmastodon.org), [Peertube](https://joinpeertube.org/), etc. instance) has its home on a server somewhere on the Internet. Our script shows if and how these instances crawled by [fediverse.observer](https://fediverse.observer) are rated by [the Green Web Foundation](https://www.thegreenwebfoundation.org/) - we let these databases collide! To enhance this with more information about the server und webhoster, we added an open research process with the goal to make greenwashing more transparent. A open research process means that our project lives through everyone's participation! ✊

<details>
  <summary>Why are we doing this?</summary>

  We, as users of the federated universe called ["Fediverse"](https://en.wikipedia.org/wiki/Fediverse) are aware that our self-hosted instances run on servers with different types of energy consumption. The global energy demand for our datacenters will continue to increase despite the fact that they will be more efficient. One of the core questions are not, what kind of energy will we use in the future, but which datacenters do not use renewable energy for their operation. Should we favor certain datacenters and services? Green webhosting will be one of the many key factors in addressing the climate crisis and environmental degradation on our planet - we face huge challenges on our way there. On the one hand, there is a great lack of transparency, including increasing greenwashing tendencies. We also face the complexity of the energy market and its unpredictable dynamics. How can we find out which energy suppliers offer us real green energy without greenwashing.

With our research we try to get a first but not complete overview about webhosting provider. This data set include Fediverse instances which are using renewable energy for their servers. We want to take a closer look at our digital infrastructure to make our ecological footprint we leave in our digital spaces more visible. With this step we try to think ecology and technical aspects together, to make them more visible and aware for everyone - our main intention is to create awareness for an ecologically sustainable development of our digital space including the Fediverse. We are aware that it is not yet possible to generate or produce green energy everywhere on our planet and we don't want to blame anyone for not using green energy. But we may want to name such companies that are complicit in the destruction of the planet. There may be some datacenter operators who claim to use renewable energy, but in the same breath support the fossil fuel industry with other technologies - here we say: not with us!
<br>
We are aware that the production of the hardware that our server uses, takes place in conditions that involve the exploitation of human and nature. Therefore, the questions are not only limited to the use of energy, but must also be extended to the dimensions of social and human rights aspects. Unfortunately, resource extraction and the manufacture of the hardware with its individual components are often disregarded. For this reason in particular, we would like to strongly advocate that this issue must be considered from a multi-perspective. Poor working conditions and exploitation of children are part of the non-transparent global supply chain.
What political measures follow from these violations against people and nature?
Here we would like to put pressure on datacenters, but also appeal to politicians to deal with the issue more intensively. On the one hand, our societies could impose legal requirements on the operators (which are not again based only on the operators' voluntary commitment). We should establish scientifically standardized methods of measuring CO² emissions that include the various Scope 1-3 emission levels - which apply to all, and from which mandatory targets and measures result.

> "An Rechenzentren müssen daher ebenso hohe Anforderungen gestellt werden, wie an andere sensible Infrastrukturen: Verfügbarkeit, Resilienz, Umweltverträglichkeit, Zukunftsfähigkeit. Trotz dieser hohen Relevanz existieren derzeit keine zuverlässigen Statistiken über Anzahl, Standort, Energieverbrauch und  Leistungsfähigkeit von Rechenzentren, weder national noch europäisch oder international." [source](https://www.umweltbundesamt.de/sites/default/files/medien/376/publikationen/politische-handlungsempfehlungen-green-cloud-computing_2020_09_07.pdf)

Building open knowledge can lead to more conscious decisions in our everyday actions. Maybe our collected information can thus lead to a more conscious decision when choosing a server - in the hope that in the future no one will use servers that are powered by harmful energy, especially fossil.
</details>

<details>
    <summary>A brief history</summary>

The project emerged from discussions in Fediverse in 2019. The discussions had a focus on digitization, climate change, the environmental sustainability of social networks, data centers and their energy consumption. It all started in 03/2019 with the mastodon-account GreenFediverse. In the beginning the community around the project created some manually edited lists: old instances list and a green webhoster list. In 2020 [@attac_hl](https://mastodon.social/@attac_hl) worked on a first technical realization of [moagee's](https://chaos.social/@moagee) idea to combine the databases from Mastostats and The Green Web Foundation to a new [Green instances list](https://greenfediverse.frama.io/green-instances/). In 2021 [@aligyie](https://digitalcourage.social/@aligyie), started to create a [green webhost ranking](https://lite.framacalc.org/green-webhost-ranking) which is still in use. In 05/2022 the fediverse instances database changed again, from [the-federation.info](https://the-federation.info/) to [nodes.fediverse.party](https://nodes.fediverse.party/) provided by [Minoru](https://functional.cafe/@minoru), and to [fediverse.observer](https://fediverse.observer/). The last allows to obtain an almost complete list of the fediverse. Since the beginning, the project has been maintained by [moagee](https://chaos.social/@moagee). There are still different people from the fediverse-community working on the project or researching and filling the database with additional information.
  </details>

----------

## About the research process:

**Scope 1-3**

<details>
  <summary>Our concern is therefore to find out whether datacenters operate in an ecologically sustainable manner. We must be aware that there are different scopes when measuring datacenter emissions.</summary>
We differ here between Scope 1-3 emissions. Even in some data center reports, information on Scope 1-3 emissions can already be found today. However, this tracking of produced emissions from corporate processes is only at the beginning of its implementation and is often not standardized in its systematics.
With our project, we can at first only determine Scope 1 emissions, where we investigate the question of whether a data center uses and purchases renewable energy for its services. At this level, it is easier to determine a company's emissions because data centers often provide information on what kind of energy they use. The other levels are subject to a more complex system we cannot provide here. The methodology is often not a uniform one, but companies voluntarily lean toward defined categories. At the level of Scope 2-3 emissions, cooling, water consumption or the manufacture of servers are determined, i.e. all emissions along the entire supply chain. In addition, we should be aware that the scope 3 emissions level, which is yet to be determined, often accounts for more than 80-90% of the actual emissions, i.e. the largest part. Scope 3 emissions are therefore often emissions from other companies, i.e. suppliers. In the overall picture, this therefore means that complete transparency is necessary and must be established in order to actually make all emissions visible at all levels. From this, a real statement would then be derived as to whether a data center is really operated in an ecologically sustainable manner. This full coverage lies in the future.
  </details>

**Make greenwashing transparent**

<details>
<summary>Greenwashing is very present</summary>

> What should be considered: Information provided by the hosting provider of the server himself should be viewed critically.

Server operators who claim to use renewable energy, but at the same time and by other means invest in fossil energies, cannot in our view make an ecologically sustainable contribution and are destroying their acquired "green" status themselves - we do this in order to bring more reality, transparency and quality into play. In simpler terms, we unfortunately have to remain suspicious of all the greenwashing and marketing promises. Finally, we refrain from an exact and hardly realizable rating, because a lot of factors have to be considered and cannot be read out technically yet. But what we can do is to document our research process transparently.

Making ethical corporate responsibility visible is itself ethical action. Statements by companies on this are thus an expression of possible ethical action. But unfortunately, this often only takes place out of motivation, if this leads to the improvement of their own image and the creation of profit. When the creation of transparency is not done out of honest conviction, this must be viewed critically, so that third parties, i.e. others need to create transparency and a understanding for this actions. It is also suggested that self-imposed responsibility would be sufficient, but this is often only intended to prevent the creation of new laws so that companies do not come under pressure and can continue to make profits undisturbed. They are creating a trade in indulgences instead of real transformation. Here, the creation of transparency of a social-ecological corporate action is important. Companies often have an interest in hiding unwelcome information like their use of fossil fuels. So, we should always consider to use third-party information if available.

However, some companies are not only focused on renewable energy. Companies often use a spin-off for this, or establish a subsidiary, for example, to cover up this practice mostly for image and marketing reasons. Open databases such as OpenCorporates lists companies in an open database, and in the best case can show us what a subsidiary belongs to. They give themselves a green image, but at the same time continue to invest in dirty energy.

To be clear, preventing CO2 in the first place must be by far our first priority. We have to move to a world, where the servers, the cooling of the servers, the production of the server, the workbuildings, and everything else electrically is powered by wind and solar. But of course, not every emitted CO2 is caused by electricity, so it is also good when hosting providers measure their CO2 footprint and provide compensation and a plan to lower their footprint in the future. Here are some examples on how the CO2 footprint can be lowered: use of refurbished hardware, use of already exisiting buildings for datacenters (e.g. ungleich.ch), use vendors known for good sustainability (e.g. hostsharing.net), use good materials when building a new datacenter, giving employees benefits when they come by bike to work, ...

</details>

<details>
<summary>Clarifying dependencies</summary>

> Server hosting is complex

To host a server you need: computers/servers, some place to put them (data center), electricity to power and cool them, good internet connection and everything doubled on a different place (backup). For a complete picture, it is recommended to also check the companies e.g. behind the internet service provider (ISP). Often, hosting providers rent a room in a datacenter by so called „colocation providers“. Which means that they consume the internet and the electricity of the datacenter and rely on their sustainability strategy.

Sometimes it helps to look for the geographical location of the server by entering its internet-address in a "whois" service (for example: [whois.domaintools.com](https://whois.domaintools.com) or [Flagfox: Geotool - IPLookUp](https://iplookup.flagfox.net/) (also available as a [Firefox add-on](https://addons.mozilla.org/de/firefox/addon/flagfox/?src=external-iplookup.flagfox.net).)

</details>

<details>
<summary>More information on the energy powering the servers</summary>

> Energy - at the end everything is about energy and where it comes from

Often, data center providers have a nicely prepared website where we can get information on the use of renewable energy. Often it is enough if we enter terms such as "renewable energy", "environment" or "sustainability" on the respective page of the hosting provider. But also in a search engine of your choice, often the name of the hosting provider in connection with these terms is enough. In the best case, we will receive a link to an energy certificate, which has been uploaded and made accessible as a .pdf. Here we then get the name of the power provider and whether it is powered with renewable energy.

Next step should be to continue with the research on the energy provider itself. Often, those providers are clarified as green but make no investment in an energy transition from nuclear and fossil-fuel energy, to wind and solar. Also hydroelectric has to be seen critical sometimes, e.g. if a new plant is built in a nature reserver.

Positive examples are e.g. Green-Planet-Energy and Naturstrom AG. Other good energy providers can be found e.g. here:

https://utopia.de/bestenlisten/die-besten-oekostrom-anbieter/#kriterien

https://greenecofriend.co.uk/eco-friendly-energy-suppliers/

A must-read to understand the negative examples and how they do greenwashing:

https://www.thegreenwebfoundation.org/what-you-need-to-register/

Often it makes sense to check which companies are ultimately receiving the money at the end. Here's an example (which is not that worse but can show the relations):

> For example, you are an administrator of a Fediverse instance. You rent a server, e.g. a VPS, on which you then install an activity pub protocol software like Mastodon and offer this service to a community. From time to time you collect donations from your community so that you don't have such high costs to run the server. This money, be it your own or your users', then goes to your webhoster on a monthly basis. Your webhoster is e.g. Biohost. Biohost gets the energy for their servers from the energy provider Polarstern, which gets the energy from the energy supplier Verbund AG and it's power plants, which in turn run hydroelectric plants. And now let's change perspective: Biohost only pays for green electricity from a chosen Polarstern tariff, Polarstern buys the energy from Verbund AG, but Verbund AG uses the profits not only to buy or sell renewable energy, but also to sell a harmful energy mix on energy markets with its specially established subsidiary. In addition, there is a construction of controversial power plants. Overall, we continue to invest indirectly in fossil energy, and each of us is part of this by giving our money to those who do not promote the socio-ecological transformation.

So we moved on with Verbund AG and continued to ask critical questions about what the company's real energy mix looks like. We can find out by entering the term: ["Stromkennzeichnung"](https://de.wikipedia.org/wiki/Stromkennzeichnung) (german) or the english term: "electricity disclosure report" on their website. In countries such as Germany or Austria, companies are required to disclose the composition of their energy mix once a year "on the annual electricity bill and on all promotional materials", also the dealer and supplier mix. These are then usually published in annual reports as .pdf on their websites. In Austria E-Control has been monitoring the implementation of electricity labelling by electricity suppliers in its capacity as supervisory authority since 2005. Also information about the CO2 emissions and radioactive waste must be published. [Here](https://www.e-control.at/publikationen/oeko-energie-und-energie-effizienz/berichte/stromkennzeichnungsbericht?inheritRedirect=true) you will find annual reports. In Great Britain it's called [„Fuel Mix Disclosure“](https://www.ofgem.gov.uk/environmental-and-social-schemes/renewables-energy-guarantees-origin-rego/energy-suppliers/fuel-mix-disclosure-fmd). So there are different legal frameworks depending on the country.

other helpers:<br>

> [Datacenter Journal](https://www.datacenterjournal.com/data-centers/)<br>
> [Data Centre Dynamics](https://www.datacenterdynamics.com/en/)<br>

</details>

To get more information on the hosting provider and their energy suppliers, sometimes you have to contact them directly via email.

------

## How does the 'Green Instances List' work?

<details>
  <summary>The database collider 💫</summary>
There are two open API we work with:<br>

> **Database 1:**
>   [The Green Web Foundation Partner API](https://admin.thegreenwebfoundation.org/api-docs/) [(GitHub)](https://github.com/thegreenwebfoundation) works in such a way that it outputs the "green" status of a website. This is done by The Green Web Foundation (GWF) first using various machine-verifiable methods to determine the domain names, IP addresses or the [autonomous system number (ASN)](https://en.wikipedia.org/wiki/Autonomous_system_(Internet)). In addition, the GWF has built an independent certificate database, where operators of websites or webhosts, which can not be automated-machine recorded, can register - take a look at the [registration-process](https://www.thegreenwebfoundation.org/what-you-need-to-register/). There, a "proof" in the form of a certificate can then be provided, i.e. a badge can be acquired. These registered servers and services can then be labeled, i.e. provided with the information that they operate with renewable energy or not. Website addresses can be queried via a ["Green Web Check"](https://www.thegreenwebfoundation.org/green-web-check/) search mask or via the API. Whether a "green" entry exists here, the API then outputs a status result with "green" or "gray". More information about the functionality of this API can be found [here](https://www.thegreenwebfoundation.org/news/introducing-the-green-web-foundation-partner-api/).
> 
> > The GWF "is a recognized not-for-profit organization registered in the Netherlands, that is run by a small group of dedicated volunteers located in Germany and The Netherlands." "Our strategy is simple: we use the existing protocols of the internet to understand where infrastructure is run, when we speak to the people running it to see how they power it. We then make these answers easy to look up, providing free online tools, APIs and open datasets, and helping people incorporate this information into their own services and analysis."

Here's a interesting [talk by Chris Adams](https://media.ccc.de/v/36c3-11113-reducing_carbon_in_the_digital_realm#t=1608) of the GWF.<br>

> **Database 2:**
>   Another database we use is the one from [fediverse.observer](https://fediverse.observer) [GitLab](https://gitlab.com/diasporg/poduptime). With the open [API](https://api.fediverse.observer/) we get a list of all connected and crawled instances of the Fediverse. A single instance offer us more information with the "/nodeinfo/2.0", (e.g. https://chaos.social/nodeinfo/2.0). Here we get, for example, what kind of "software" (Mastodon, PeerTube, etc.) is used on an instance.

**Then the magic begins: The database collider:
Then we let these two databases collide with each other. The python-script [(instances.py)](https://codeberg.org/GreenFediverse/green-instances/instances.py) then executes the command to green check all fediverse instances. The result is our [Green Instances List](https://greenfediverse.codeberg.page/green-instances/).

</details>

## How to add your research:

> [Create an account](https://codeberg.org/user/sing_up) here on Codeberg to work in this repository

> Paste your research into the respective files, inside of ["/additional-info/..."](https://codeberg.org/GreenFediverse/green-instances/src/branch/master/additional-info). Whether you want to add a "info" or mark a webhoster as "rejected", both kind of texts are stored in this folder. Can't find the respective file? Created a new .md file with a simple name, ideally as the exact >provider< name from the [Green instances list](https://greenfediverse.codeberg.page/green-instances/).

[Please use this format for your research.](https://codeberg.org/GreenFediverse/green-instances/src/branch/master/stylesheet.md?display=source)


> Is this done, edit the [/"table.js"](https://codeberg.org/GreenFediverse/green-instances/src/branch/master/table.js) to connect your edited or created file to the list. In table.js you'll find two lists at the top of the code: "rejected" and "additional-info". Please note the grey written comments how to insert the names correctly.

> At the end your changes need to be pushed into the "pages" branch - the community will check the changes and the admins will commit the changes from time to time.

## Fediverse instance registration

If you can't find your instance in our list, you should visit [fediverse.observer](https://fediverse.observer) and register your instance there. If your instance is registered there, but The Green Web Foundation won't give you a result for your instance (server), then you should visit The Green Web Foundation [Member Portal](https://admin.thegreenwebfoundation.org) and register your instance (website) there. There you can add the datacenter and hosting provider with it's ASN and IP-ranges you use. See also their [support website](https://www.thegreenwebfoundation.org/support/).

--------

## Contact:

To get in touch with the project, you can create an account here on [Codeberg](https://codeberg.org/user/sing_up), write us on [Mastodon](https://chaos.social/@greenfediverse) or visit us in our
[Matrix room](https://matrix.to/#/#greenfediverse:matrix.org).

You can help build the Green Instances List, work with the APIs (instances.py), or building our open database through research. In all cases, always check the [open issues](https://codeberg.org/GreenFediverse/green-instances/issues) in our repository first to see what tasks are open, and where we are in the process.<br>

We are on:<br>
[Hackers against climate change](https://hacc.uber.space/GreenFediverse)<br>
[JoinFediverse.wiki](https://joinfediverse.wiki/GreenFediverse)<br>

We are part of the movement!<br>
<br>
<a href="https://bits-und-baeume.org" target="_blank">
  <img src="https://bits-und-baeume.org/assets/images/bits-und-baeume-logo_CCBY-schauschau.cc.svg" alt="Bits & Bäume Movement"><br>

------

## Contribution & ☕-fund

You can support us by filling our ☕ fund on [ko-fi](https://ko-fi.com/greenfediverse)! Coffee for all who contribute!
This list is for those who are helping out and keeping this project alive! Thank you for your support! 💚✊ In this way we would like to value the work that everyone does for this project:

<details>
    <summary>Contributors:</summary>

> - **[moagee](https://chaos.social/@moagee):** maintains the GreenFediverse project<br>
> - **[aligyie](https://digitalcourage.social/@aligyie):** is working on the green webhost ranking<br>
> - **[lorenz](https://codeberg.org/lorenz.josten):** helped out modifying the code<br>
> - **[attac_hl](https://mastodon.social/@attac_hl):** wrote the first version of the code<br>
> - **[Minoru](https://functional.cafe/@minoru):** modified the "instances.py"<br>
> - **[Silmathoron](https://floss.social/@silmathoron):** helped to create a matrix room with necessary settings<br>
> - **[Joel](https://mastodon.social/@Jolg42):** rewrote the instances.py (graphql/python code) to fetch instances from fediverse.observer<br>

We use different infrastructure services. In this sense, thanks to:

> - **[Codeberg](https://codeberg.org)** for hosting our repository<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/codeberg.org?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>
> 
> - **[chaos.social](https://chaos.social)** for hosting our mastodon-account<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/chaos.social?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>
> 
> - **[Paula](https://climatejustice.social/@PaulaToThePeople)** for hosting our [joinFediverse.wiki](https://joinfediverse.wiki/GreenFediverse)<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/joinfediverse.wiki?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>
> 
> - **[The Green Web Foundation](https://www.thegreenwebfoundation.org/)** to green check our fediverse instances with their API<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/admin.thegreenwebfoundation.org?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>
> 
> - 🔭 **[fediverse.observer](https://fediverse.observer)** for crawling through our federated universe in search of new planets<br>
>   
>   <a href="https://www.thegreenwebfoundation.org/green-web-check/?url=fediverse.observer" target="_blank">
>   <img src="https://www.thegreenwebfoundation.org/wp-content/plugins/wp-green-checker/public/img/green-web-smiley-bad.svg" width="100" height="100"></a><br>
> 
> - 🔭 **[nodes.fediverse.party](https://nodes.fediverse.party/)** for use of the instances list in the past<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/nodes.fediverse.party?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>
> 
> - 🔭 **[the-federation.info](https://the-federation.info/)** for use of the instances list in the past<br>
>   <img src="https://api.thegreenwebfoundation.org/greencheckimage/the-federation.info?nocache=true" alt="This website is hosted Green - checked by thegreenwebfoundation.org"><br>

</details>

-------

The Green Instances List (pages website) is hosted by Codeberg on a [Individual Network Berlin e.V.](https://in-berlin.de/provider/colo.html) server 💚<br>

-------

<details>

<summary>Licence and third-party code:</summary>

> - The third-party database from the GreenWebFoundation and also the code and database of this repository
> are published under the Open Database Licence [ODbL 1.0 license](https://opendatacommons.org/licenses/odbl/summary/index.html)
> - The code in [snarkdown.js](https://codeberg.org/GreenFediverse/green-instances/src/branch/master/snarkdown.js) is taken from [developit/snarkdown](https://github.com/developit/snarkdown) and is published under [MIT license](https://github.com/developit/snarkdown/blob/main/LICENSE)

</details>