import time
from datetime import datetime
from urllib.error import URLError, HTTPError
from urllib.parse import quote
import urllib.request
import http.client
import json
import random
import sys

URL_INSTANCES = "https://nodes.fediverse.party/nodes.json"
URL_GWF = "https://api.thegreenwebfoundation.org/greencheck/%s"

JSON_OUTPUT_FILE = "./instances.json"


def load_instances():
    print(f"Fetching fediverse instances.")
    instances_request = urllib.request.Request(
        url=URL_INSTANCES,
        method="GET"
    )
    raw_data = download_retry(instances_request).decode()
    json_data = json.loads(raw_data)
    return json_data


def download_retry(req, params=None, timeout=60):
    try:
        try:
            return urllib.request.urlopen(req, timeout=timeout).read()
        except HTTPError as e:
            if e.code in [403, 404]:
                return ""
            else:
                raise
    except (URLError, http.client.BadStatusLine):
        print("Connection to %s failed. Retrying..." % req["url"])
        time.sleep(random.uniform(1.0, 3.0))
        return download_retry(req["url"], params)
    except Exception as e:
        print("Error on %s: '%s'. Retrying..." % (req["url"], e))
        time.sleep(random.uniform(1.0, 3.0))
        return download_retry(req["url"], params)


def green_check(instances):
    print(f"GWF checking {len(instances)} fediverse instances.")
    instances_meta_data = []
    counter = 0
    start_time = datetime.now()
    for instance in instances:
        instance_url = URL_GWF % quote(instance.encode("UTF-8").strip())
        green_check_request = urllib.request.Request(
            url=instance_url,
            method="GET"
        )
        gwf = json.loads(download_retry(green_check_request))

        if "error" in gwf:
            print(f"'url {instance}' raised GWF error: {gwf['error']}")
            if gwf['error'] == "Invalid url":
                # Instances with invalid URL are usually down, skipping...
                continue

        instance_meta_data = {
            "name": instance,
            "AS": "Unknown",
            "ASN": "Unknown",
            "green": -1,
        }

        if gwf["green"]:
            instance_meta_data["green"] = 1
        else:
            instance_meta_data["green"] = 0

        if "hosted_by" in gwf:
            instance_meta_data["AS"] = gwf["hosted_by"]

        instances_meta_data.append(instance_meta_data)
        counter += 1
        show_progress(start_time, counter, len(instances))

    return instances_meta_data


def show_progress(start_time, counter, total_count):
    progress = counter / total_count * 100
    time_elapsed = datetime.now() - start_time
    time_estimated = (time_elapsed / progress) * (100 - progress)
    progress_formatted = "{:.2f}".format(progress)
    time_elapsed_formatted = str(time_elapsed).split('.')[0]
    time_estimated_formatted = str(time_estimated).split('.')[0]
    sys.stdout.write("\r")
    sys.stdout.write(f"Progress: {progress_formatted}%, Time elapsed | remaining: {time_elapsed_formatted} | {time_estimated_formatted}")


def write_to_json_file(data):
    with open(JSON_OUTPUT_FILE, "w") as f:
        timestamp = int(datetime.timestamp(datetime.now()))
        json.dump({'update': timestamp, 'instances': data}, f)


def main():
    instances = load_instances()
    meta_data = green_check(instances)
    write_to_json_file(meta_data)


if __name__ == '__main__':
    main()

