var sort_idx = 0;

/*
Rejectlist >rejected<:
Here you can enter the provider you want to be listed as rejected.
Note:
the left name is the exact displayed >provider< name from the >Green instances list<,
the right "name" is the exact file name stored in the /additional-info folder.
*/
const REJECT_LIST = {
    "1&1": "1&1",
    "Amazon EU (Ireland)": "Amazon",
    "Amazon EU (Frankfurt)": "Amazon",
    "Amazon US West": "Amazon",
    "Amazon Montreal": "Amazon",
    "Cloudflare": "Cloudflare",
    "CJ2 Hosting BV": "CJ2 Hosting BV",
    "FAELIX": "FAELIX",
    "Google Inc.": "Google Inc",
    "Ovh": "Ovh",
    "Firebase Hosting": "Firebase Hosting",
    "Microsoft Azure": "Microsoft Azure",
    "Microsoft Azure USA": "Microsoft Azure",

    /*don't forget the "," at the end*/
}

/*
additional-info for "green" checked instances and self-made acceptlist: >INFO<:
Here you can connect your edited or created .md files to the list.
Note:
the left name is the exact displayed "provider-name" from the >Green instances list<,
the right name is the exact "file-name".md, you stored in the /additional-info/... folder.
*/
const ADDITIONAL_INFO_LIST = {
    /*INSTANCES ACCEPTLIST:
    "instance-name": "file-name", */
    "scicomm.xyz": "scicomm.xyz",
    "mamot.fr": "mamot.fr",
    "digitalcourage.social": "digitalcourage.social",
    /*WEBHOSTER/PROVIDER:
    "provider-name": "file-name", */
    "1984 Hosting Company": "1984 Hosting Company",
    "A2 Hosting": "A2 Hosting",
    "Alfahosting GmbH": "Alfahosting GmbH",
    "Artfiles New Media GmbH": "Artfiles New Media GmbH",
    "Akamai Technologies, Inc.": "Akamai Technologies, Inc.",
    "bHosted.nl": "bHosted.nl",
    "biohost": "biohost",
    "BIT": "BIT",
    "Bitency": "Bitency",
    "Blix Solutions AS": "Blix Solutions AS",
    "ColoCenter bv": "ColoCenter bv",
    "Combell Group NV": "Combell Group NV",
    "Clouding": "Clouding",
    "Configo Systems GmbH": "Configo Systems GmbH",
    "cyon": "cyon",
    "Digital Pacific": "Digital Pacific",
    "dogado": "dogado",
    "dreamhost": "dreamhost",
    "Dream Host": "Dream Host",
    "Eco Hosting": "Eco Hosting",
    "exo.cat": "exo.cat",
    "Equinix.com": "Equinix.com",
    "FlokiNET": "FlokiNET",
    "Firstcolo": "Firstcolo",
    "Freethought Internet": "Freethought Internet",
    "Greenhost": "Greenhost",
    "goneo Internet GmbH": "goneo Internet GmbH",
    "Hetzner Online AG": "Hetzner Online AG",
    "Host Europe GmbH": "Host Europe GmbH",
    "hosting.de GmbH": "hosting.de GmbH",
    "Hostsharing eG": "Hostsharing eG",
    "hosttech Schweiz": "hosttech Schweiz",
    "I3D.net": "I3D.net",
    "Infomaniak": "Infomaniak",
    "Ikoula": "Ikoula",
    "Init Seven AG": "Init Seven AG",
    "iWeb": "iWeb",
    "IONOS SE": "IONOS SE",
    "Infra Blocks B.V.": "Infra Blocks B.V.",
    "Internet AG": "Internet AG",
    "Krystal Hosting": "Krystal Hosting",
    "LeaseWeb": "LeaseWeb",
    "LiteServer B.V.": "LiteServer B.V.",
    "LYF Solutions": "LYF Solutions",
    "Loopia AB": "Loopia AB",
    "mailbox.org": "mailbox.org",
    "manitu GmbH": "manitu GmbH",
    "meerfarbig GmbH & Co. KG": "meerfarbig GmbH & Co. KG",
    "METANET": "METANET",
    "Metaregistrar BV": "Metaregistrar BV",
    "minuskel screen partner GmbH": "minuskel screen partner GmbH",
    "Mijndomein Hosting BV": "Mijndomein Hosting BV",
    "Mittwald": "Mittwald",
    "netcup GmbH": "netcup GmbH",
    "next layer Telekommunikationsdienstleistungs- und BeratungsGmbH": "next layer Telekommunikationsdienstleistungs- und BeratungsGmbH",
    "Nine Internet Solutions AG": "Nine Internet Solutions AG",
    "NovaTrend Services GmbH": "NovaTrend Services GmbH",
    "NMM RZ DD": "NMM RZ DD",
    "NTS Workspace AG": "NTS Workspace AG",
    "Neostrada b.v.": "Neostrada b.v.",
    "Oderland Webbhotell AB": "Oderland Webbhotell AB",
    "One.com": "One.com",
    "oriented.net GmbH": "oriented.net GmbH",
    "PCextreme B.V.": "PCextreme B.V.",
    "PlanetHoster": "PlanetHoster",
    "Plusserver": "Plusserver",
    "ProcoliX": "ProcoliX",
    "Proserve": "Proserve",
    "Provisov": "Provisov",
    "Rackspace UK": "Rackspace UK",
    "SAP": "SAP",
    "Serverius": "Serverius",
    "Solcon Internetdiensten BV": "Solcon Internetdiensten BV",
    "Strato AG": "Strato AG",
    "SJM Steffann": "SJM Steffann",
    "Savvii - Managed Hosting": "Savvii - Managed Hosting",
    "Surfplanet GmbH": "Surfplanet GmbH",
    "Teuto.net": "Teuto.net",
    "Tilaa": "Tilaa",
    "The Positive Internet Company Ltd.": "The Positive Internet Company Ltd.",
    "SysEleven GmbH": "SysEleven GmbH",
    "TransIP B.V": "TransIP B.V",
    "TransIP B.V.": "TransIP B.V",
    "Uberspace": "Uberspace",
    "UD Media GmbH": "UD Media GmbH",
    "Ungleich": "Ungleich",
    "Ungliech": "Ungleich",
    "VCServer Network": "VCServer Network",
    "vikings": "vikings",
    "Vimexx": "Vimexx",
    "VPS GREEN": "VPS GREEN",
    "webgo GmbH": "webgo GmbH",
    "WebhostOne GmbH": "WebhostOne GmbH",
    "Web Hosting Canada": "Web Hosting Canada",
    "Worldstream Internet Solutions": "Worldstream Internet Solutions",
    "World4You": "World4You",
    "WP-Projects": "WP-Projects",
    "XMission": "XMission",
    "XS4ALL": "XS4ALL",

    /*don't forget the "," at the end of the line, otherwise the list will crash and won't load*/
}

/*
this is our update line that tells us when the list was last fetched. (LAST UPDATED)
*/
function show_last_updated(instances) {
    var a = new Date(1000 * instances['update']);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = (a.getFullYear() + "").padStart(2, "0");
    var month = months[a.getMonth()];
    var day = a.getDate();
    var hour = (a.getHours() + "").padStart(2, "0");
    var min = (a.getMinutes() + "").padStart(2, "0");
    var sec = (a.getSeconds() + "").padStart(2, "0");
    var datetime = day + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    document.querySelector("body > p").innerHTML = "LAST UPDATED: " + datetime;
}

/*
this function loads the entries (.md files) from the reject list.
It can also be changed to .html but we want to use .md
*/
function load_rejectlist() {
    var rejected = {}
    for (let [key, value] of Object.entries(REJECT_LIST)) {
        if (!rejected.hasOwnProperty(value)) {
            rejected[value] = "";
            load_html("additional-info/" + value + ".md", function (md) {
                rejected[value] = md;
            })
        }
    }
    return rejected;
}

/*
This function loads the entries (.md files) from the acceptlist list.
You won't find the acceptlist in here.
It's in the repository: acceptlist/instances.json where you can set an instance to green.
*/
function load_acceptlist() {
    var accepted = []
    load_json("acceptlist/instances.json", function (jsonStr) {
        JSON.parse(jsonStr).forEach((instance) => {
            accepted.push(instance);
        });
    });
    return accepted;
}

/*
this function loads the entries (.html files) from the additional list.
It can also be changed to .html
*/
function load_additional_info() {
    var additional_info = {};
    for (let [key, value] of Object.entries(ADDITIONAL_INFO_LIST)) {
        if (!additional_info.hasOwnProperty(key)) {
            additional_info[key] = "";
            load_html("additional-info/" + value + ".md", function (md) {additional_info[key] = md;})
        }
    }
    return additional_info;
}

/* ... */
function load_html(file, callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType('text/md');
    xobj.open('GET', file, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState === 4 && xobj.status === 200) {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

/* ... */
function load_instances(callback) {
    load_json('instances.json',
    text => {instances = JSON.parse(text); callback(instances);});
}

/* ... */
function load_json(file, callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType('application/json');
    xobj.open('GET', file, true);
    xobj.onreadystatechange = function () {
        if (xobj.readyState === 4 && xobj.status === 200) {
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

/* creates the table header and sorting */
function create_table_header() {
    var thead = document.querySelector("thead tr");
    for (var i = 0; i < thead.cells.length; i++) {
        thead.cells[i].className = "";
        if (i+1 == sort_idx) {
            thead.cells[i].classList.add("sortasc");
        }
        else if (i+1 == -sort_idx) {
            thead.cells[i].classList.add("sortdesc");
        }
    }
}

/* creates the table rows, begins with 0, sets the names and parameters */
function append_instance(instance, rejected_instances, accepted_instances, additional_info) {
    if (instance['name'] == "0") return;
    if (instance['metaimage'] == "0") return;
    row = create_row();
    row.className = "";

    /* instance name */
    row.cells[0].innerHTML = '<a href="https://' + instance['name'] + '/" target="_blank">' + instance['name'] + '</a>';

    /* protocol */
    row.cells[1].innerHTML = instance['type'];

    /* country/location */
    row.cells[2].innerHTML = instance['country'];

    /* server/provider fetched as AS and ASN (not from nodeinfo) */
    row.cells[3].innerHTML = instance['AS']; // + " (" + i['ASN'] + ")";

    /* signup */
    row.cells[6].innerHTML = instance['signup'];

    /* users */
    row.cells[7].innerHTML = instance['total_users'];

/* To create a new row in the table, above is the last position! Don't forget to add a new line <th>NAME</th> in index.html*/

    /* if there is a instance-'name' in the accept-list, then they are "green" */
    if (accepted_instances.includes(instance['name'])) {
        row.classList.add("green");
        row.cells[4].innerHTML = "green";

    /*if there is no data = -1 from the GWF, the instance is displayed as grey*/
    } else if (instance['green'] === -1) {
        row.classList.add("nodata");
        row.cells[4].innerHTML = "grey (no data)";

    /*if there is data from the GWF but not green = 0 from the GWF, the instance is displayed as grey*/
    } else if (instance['green'] === 0) {
        row.classList.add("notgreen");
        row.cells[4].innerHTML = "grey";

    /*if there is no data about the server/provider (AS) and no check by the GWF = "" the instance is displayed as grey and "unknown"*/
    } else if (instance['AS'] === "") {
        row.classList.add("notgreen");
        row.cells[4].innerHTML = " ";

    /*if there is data = 1 from the GWF, the instance is displayed as "green"*/
    } else if (instance['green'] === 1) {

    /*if the server/provider (AS) is rejected (means on the rejected-list),
    the instance is displayed with the red word: >rejected<*/
        if (REJECT_LIST.hasOwnProperty(instance['AS'])) {
            row.classList.add("rejected");
            row.cells[4].innerHTML = '<a href="javascript:void(0)">NOT GREEN - REJECTED</a>';

        } else {
            row.classList.add("green");
            row.cells[4].innerHTML = "GREEN 💚✊";
        }

    }

    /*if rejected instance, active rejected link*/
    if (REJECT_LIST.hasOwnProperty(instance['AS'])) {
        row.cells[4].addEventListener("click", function () {
            var reject_md = rejected_instances[REJECT_LIST[instance['AS']]];
            var el = document.getElementById("reject");
            el.firstChild.innerHTML = reject_md;
            el.classList.add("shown");
        });
    }

    /*if there is a additional-info for an instance, active >info< link will be displayed*/
    if (ADDITIONAL_INFO_LIST.hasOwnProperty(instance['AS']) || ADDITIONAL_INFO_LIST.hasOwnProperty(instance['name'])) {
        row.cells[5].innerHTML = '<a href="javascript:void(0)">INFO</a>';
        row.cells[5].addEventListener("click", () => {
            row.classList.add("additional-info");
            var el = document.getElementById("additional-info")
            if (additional_info.hasOwnProperty(instance['name'])) {
                el.firstChild.innerHTML = additional_info[instance['name']]
            } else if(additional_info.hasOwnProperty(instance['AS'])) {
                el.firstChild.innerHTML = additional_info[instance['AS']];
            }
            el.classList.add("shown");
        });
    }
}

    /*table body*/
function create_row() {
    tb = document.querySelector("table tbody");
    row = tb.insertRow(-1)
    /*how many rows do we need?*/
    for (let i = 0; i < 9; i++) {
        row.insertCell(-1);
    }
    return row;
}

function sort_data(instances, idx) {
    var keys = ['name','type','country','AS','green','signup','total_users'];
    var key = keys[idx];
    if (idx+1 == Math.abs(sort_idx)) {
        sort_idx = -sort_idx;
    } else {
        sort_idx = idx+1;
    }
    instances.instances.sort((a,b) => {
        a = (key == 'green') ? (1-Math.max(0,a[key])) : a[key];
        b = (key == 'green') ? (1-Math.max(0,b[key])) : b[key];
        if (a > b) {
            return Math.sign(sort_idx);
        } else if (a == b) {
            return 0;
        } else {
            return -Math.sign(sort_idx);
        }
    });
}

window.addEventListener("load", () => {
    var reject_cell = document.getElementById("reject");
    reject_cell.addEventListener("click", (evt) => {
        reject_cell.classList.remove("shown");
    });

    var additional_info_cell = document.getElementById("additional-info");
    additional_info_cell.addEventListener("click", (evt) => {
        additional_info_cell.classList.remove("shown");
    })

    rejected_instances = load_rejectlist();
    accepted_instances = load_acceptlist();
    additional_info = load_additional_info();

    load_instances((instances) => {
        show_last_updated(instances);
        create_table_header(instances);
        sort_data(instances, 0);
        create_table_body(instances, rejected_instances, accepted_instances, additional_info);

        document.querySelector("thead tr").addEventListener("click", (evt) => {
            sort_data(instances, evt.target.cellIndex);
            clear_table_body();
            create_table_body(instances, rejected_instances, accepted_instances, additional_info);
        });
    });
});

function clear_table_body() {
    tb = document.querySelector("table tbody");
    tb.innerHTML = '';
}

function create_table_body(instances, rejected_instances, accepted_instances, additional_info) {
    instances.instances.forEach(async (instance) => {
        await new Promise(resolve => resolve(
            append_instance(instance, rejected_instances, accepted_instances, additional_info))
        );
    });
}
