<p><strong>Hosting Provider:</strong> <a href="https://www.ovhcloud.com/en/" target="_blank">Ovh</a> (rejected - partly green)</p>
<p><strong>Published green energy certificate:</strong></p>
<p>not available</p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
<p>&quot;From our earliest days, we have been committed to sustainability reducing IT components waste, optimizing data
    center energy consumption, innovating for more efficient cooling systems.&quot; (<a
            href="https://corporate.ovhcloud.com/en/sustainability/" target="_blank">source</a>)</p>
<p>&quot;We work closely with our technology suppliers to address issues of stability, performance, efficiency and
    durability. As part of this commitment, we are able to describe our overall environmental impact not only in terms
    of greenhouse gases emissions (GHGs), but also energy, water, waste, and transportation management - all with great
    precision.&quot;</p>
<p>&quot;1,10-1,30 Power Usage Effectiveness, 0,17-0,20 L/kWh IT Water Usage Effectiveness, 0,15-018T co²e/MWh Carbon
    Usage Effectiveness, 78% Renewable Energy Factor, 34% Reused components ratio.&quot; (<a
            href="https://corporate.ovhcloud.com/en/sustainability/" target="_blank">source</a>) (last retrieved: 01/2022)</p>
<p>Different numbers and statements in their <a
        href="https://corporate.ovhcloud.com/sites/default/files/2021-09/Environmental%20policy.pdf" target="_blank">Environmental
    Policy (.pdf)</a> by 2021.
<p>You can read in their <a href="https://corporate.ovhcloud.com/sites/default/files/2021-10/EN_Energy_Policy.pdf" target="_blank">Energy
    Policy (.pdf)</a> what they want to reach in the future.</p>
<p><em>About the energy:</em></p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>