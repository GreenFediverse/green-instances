<p><strong>Hosting Provider:</strong> <a href="https://greenhost.net" target="_blank">Greenhost</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p>-not available-</p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
<p style="text-align: justify">
    &quot;green hosting at Greenhost means more than just green energy and CO₂ compensation: from the start, we have
    aimed at using as little energy as
    possible and this is still crucial in every decision we make. By openly publishing our Sustainability Report, we
    wish to showcase how Greenhost isn&#39;t just a name&quot; <a href="https://greenhost.net/sustainable/" target="_blank">(source)</a>.
    &quot;At Greenhost we practice the three &quot;R&quot; words of waste management: Reduce, Reuse, and Recycle.&quot;
    &quot;<a href="https://www.ironmountain.com/digital-transformation/data-centers/locations/amsterdam-data-center" target="_blank">Iron
        Mountain</a> (formerly Evoswitch) located near Amsterdam is our main infrastructure provider. We host over
    20,000 websites on our own hardware colocated in the data centre. For our Human Rights work in Southeast Asia we are
    colocated
    with our own hardware in a Telstra data centre. For work in the Americas we make use of <a
            href="https://colohouse.com/miami/" target="_blank">Colohouse</a>, located in
    Miami, close to South America. The last two data centres are less focused on sustainability than our Amsterdam data
    centre. Unfortunately, we did not have a lot of choice in that matter. Our priority was finding two data centres in
    spots that would enable us to provide a fast and stable connection for human rights defenders in Southeast Asia and
    the American continent. This is an area of continued attention to us.</p>
<p><em>About the energy:</em></p>
<p style="text-align: justify">
    Greenhost uses a data center in Amsterdam 100% Dutch wind energy, Power Usage Effectiveness (PUE) of the <a
        href="https://www.ironmountain.com/data-centers/locations/amsterdam-data-center" target="_blank">server in Amsterdam</a>: 1.2,
    83% = very efficient. &quot;We chose <a href="https://pure-energie.nl/" target="_blank">Pure Energie</a> as our office electricity provider, because they:
    Provide us with a sustainable mix of Dutch wind (98.6%) and
    solar (1.4%) energy; their investment strategy is sound - a significant amount of their profits are invested in
    sustainable energy sources.&quot;</p>
<p>Pure Energie only sells electricity that it has generated itself. Map of power-plants <a
        href="https://pure-energie.nl/energiebronnen/" target="_blank">Energiebronnen</a></p>
<p style="text-align: justify">
    Greenhost also uses <a href="https://greenhost.net/sustainable/energy" target="_blank">two other data centers</a> for their services:
    1x Southeast Asia: 1.5, 67% = efficient, <a
            href="https://www.telstra.com.sg/en/products/cloud/colocation/singapore-data-centre#" target="_blank">Singapore Data
        Centre</a>, the majority of <a href="https://en.wikipedia.org/wiki/Energy_in_Singapore" target="_blank">electricity in
        Singapore</a> comes from fossil energies. 1x Miami: 1.7, 50% = average, <a href="https://colohouse.com/miami/" target="_blank">ColoHouse
        Miami</a> &quot;is the leading Miami colocation and interconnection provider.&quot; Data about renewable energy
    use is not available here. It is highly probable that this data center does not use renewable energy, <a
            href="https://en.wikipedia.org/wiki/List_of_power_stations_in_Florida" target="_blank">in this region</a> they mainly use
    natural gas, nuclear and coal.</p>
<p>This means, Greenhost is not a 100% green energy webhoster, but all Fediverse instances have their home on the green
    dutch server.</p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>
<p style="text-align: justify">
    Greenhost are partners of <a href="https://www.thegreenwebfoundation.org/" target="_blank">TheGreenWebFoundation</a>
    - GreenFediverse uses TheGreenWebFoundation API (database) to greencheck the instances.</p>
