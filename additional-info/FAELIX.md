<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
	<title></title>
	<meta name="generator" content="LibreOffice 7.2.1.2 (Windows)"/>
	<meta name="created" content="00:00:00"/>
	<meta name="changed" content="2022-01-27T20:08:13.318000000"/>
</head>
<body lang="de-DE" dir="ltr"><p><strong>Hosting Provider:</strong>
<a href="https://faelix.net/" target="_blank">Faelix</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p>-not available-</p>
<p>Entry for Faelix can not be found in <a href="https://www.thegreenwebfoundation.org/?s=FAELIX" target="_blank">GreenWebCheck</a>
of TheGreenWebFoundation, also not in section &quot;switzerland&quot;
in <a href="https://www.thegreenwebfoundation.org/directory/" target="_blank">hosting
directory</a>.</p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data centers:</em></p>
<p>&quot;Slowly the cloud is turning green by pledging to become
carbon neutral within the next few years. We've achieved that
already, and so all hosting we offer is powered by renewable energy.&quot;</p>
<p>&quot;We offer <a href="https://faelix.net/hosting/data-centre-colocation/" target="_blank">colocation
services</a> in&quot;:</p>
<ul>
	<li><p><a href="https://www.equinix.com/data-centers/europe-colocation/united-kingdom-colocation/manchester-data-centers/ma2" target="_blank">Reynolds
	House, Manchester</a> by <a href="https://sustainability.equinix.com/map-of-initiatives/" target="_blank">Equinix</a>:</p>
	<p>Eqinix - MA2 is using a &quot;Renewable Energy Coverage: 100%
	from GP - <a href="https://www.ofgem.gov.uk/environmental-and-social-schemes/renewables-energy-guarantees-origin-rego/energy-suppliers/fuel-mix-disclosure-fmd" target="_blank">REGO</a>s&quot;
		</p>
</ul>
<ul>
	<li><p><a href="https://faelix.net/news/201609/build-in-geneva/" target="_blank">Geneva,
	Switzerland</a>:</p>
	<p>Renewable energy source comes from: &quot;<a href="https://ww2.sig-ge.ch/en/a-propos-de-sig/nous-connaitre/nos-activites" target="_blank">SIG</a>
	is a Swiss provider of local services. It supplies 225,000 customers
	throughout the canton of Geneva with water, gas, electricity and
	thermal energy. It processes wastewater, recovers waste and offers
	services in the fields of energy and telecommunications.&quot; &quot;100%
	of our electricity is 100% renewable and Swiss.&quot;</p>
</ul>
<ul>
	<li><p><a href="../additional-info" target="_blank">AQL DC2, Leeds</a>:</p>
	<p align="left"><a href="https://aql.com/about/our-green-credentials/" target="_blank">AQL</a>
	names different &bdquo;Green Commitments&ldquo;, but it seems that
	they are not using renewable energy. &bdquo;Protect woodland: For
	every rack footprint utilised, will purchase and protect an acre of
	ancient woodland. So far, hundreds of acres of unspoilt forest has
	been protected. Reduce carbon footprint: Continue to measure and
	reduce our carbon footprint, seek alternative renewable energy
	solutions and improve efficiencies. Reduce waste: Reduce our waste
	as a company, reuse and recycle where possible. Develop green
	initiatives: Develop and deliver green initiatives to help educate
	staff and customers on environmental issues to help reduce personal
	carbon footprint. Adopt reuse strategies: Adopt novel property reuse
	strategies, including building datacentres in listed structures or
	as part of mixed use schemes where energy by-products can be
	recycled. Efficiency by design: Achieve efficiency by design. Ensure
	our network does not generate &ldquo;hidden carbon&rdquo;.&ldquo;</p>
</ul>
<ul>
	<li><p><a href="../additional-info" target="_blank">Telehouse North,
	London</a> 
	</p>
</ul>
<ul>
	<li><p><a href="../additional-info" target="_blank">Telehouse West,
	London</a> 
	</p>
</ul>
<ul>
	<li><p><a href="../additional-info" target="_blank">Interxion,
	London</a> 
	</p>
</ul>
<p><em>About the energy:</em></p>
<p>&quot;Our hosting infrastructure uses 100% green electricity. We
choose technologies to help us get the most efficiency from our
energy usage. We reduce, reuse, and recycle as much as we can. Many
large cloud hosting providers have made pledges to become carbon
neutral within the next few years. We are ahead of all of them: since
2016 our entire network and both our server farms are powered
entirely by wind, water, or other renewable energy.&quot; <a href="https://faelix.net/about/charter/" target="_blank">(source)</a>
</p>
<p><strong>Further research:</strong></p>
<ul>
	<li><p>missing certificate 
	</p>
	<li><p>which renewable energy provider? 
	</p>
	<li><p>Leeds and London unknown</p>
</ul>
</body>
</html>