<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" media="all" href="style.css" />
    <title>Dreamhost</title>
</head>
<body>

<p><strong>Hosting Provider:</strong></p>

Dreamhost

<p><strong>Published green energy certificate:</strong></p>

<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>

“DreamCompute is located in our Ashburn, Virginia data center, and DreamObjects is located in our Irvine, California data center.” source. Their other hosting options (VPS, dedidcated servers) might or might not also be located in these data centers (it is unspecified on the Dreamhost website).

<p><em>About the energy:</em></p>

“Partners in state-level "clean wind" programs” and “Powered by grids that obtain electricity from many renewable sources”. These are fairly empty statements. Simply buying power from the California or Virginia power grids will inherently involve "many renewable sources" (in constantly varying degrees depending on time of day and grid demand). source

<p><strong>Further research:</strong></p>

<p><strong>Other notes:</strong></p>

None of the Dreamhost blogs, press releases, or knowledge base articles provides more information on their commitment to being a green hosting provider, or anything about their data centers.

</body>
</html>