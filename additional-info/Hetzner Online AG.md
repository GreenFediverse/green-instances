<p><strong>Hosting Provider:</strong> <a href="https://www.hetzner.com/">Hetzner Online AG</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p><a href="https://www.hetzner.com/de/pdf/oekostrom_zertifikat.pdf">General certificate by NaturEnergie</a></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
<p><em>About the energy:</em></p>
<p>"<a href="https://www.naturenergie.de/">NaturEnergie</a> is exclusively electricity from 100% <a href="https://en.wikipedia.org/wiki/Hydroelectricity">hydropower</a>
    from our <a href="https://www.energiedienst.de/kraftwerke/wasserkraft/">hydropower plants on the High Rhine</a>."
    <a href="https://www.naturenergie.de/oekostrom/stromqualitaet/">(source)</a>. The <a
            href="https://de.wikipedia.org/wiki/Energiedienst_Holding#Kraftwerke">different hydropower-plants</a> are
    operated by <a href="https://de.wikipedia.org/wiki/Energiedienst_Holding">EnergieDienste Holding AG</a>. The company
    still sells energy from fossil sources, even at a decreasing rate, see: Electricity labeling <a
            href="https://www.naturenergie.de/fileadmin/naturenergie/Strommkennzeichen/2021_Stromkennzeichnungen_allgemein_MIX-2-seitig-web.pdf">Stromkennzeichnung</a>
    and comparison of annual reports <a
            href="https://www.naturenergie.de/fileadmin/naturenergie/Bilder/Strom/Stromkennzeichnungen_allgemein_MIX_2021.pdf">2019</a>
    and
    <a href="https://www.naturenergie.de/fileadmin/naturenergie/Strommkennzeichen/2021_Stromkennzeichnungen_allgemein_MIX-2-seitig-web.pdf">2020</a>.
    See also <a href="https://web.archive.org/web/20210305105317/http://www.lands-concepts.com/2011/05/naturenergie-lieber-ohne-kritik/">http://www.lands-concepts.com/2011/05/naturenergie-lieber-ohne-kritik/#more-110/</a>
</p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>