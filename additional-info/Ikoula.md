<p><strong>Hosting Provider:</strong> <a href="https://www.ikoula.com/en">Ikoula</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p>-not available-</p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p>In a whois-check we found out that most Fediverse Instances which use &quot;Ikoula&quot; as their hosting-provider
    are running on OVH servers. OVH (see our research)</p>
<p><em>About the data center:</em></p>
<p>&quot;IKOULA, a specialist in web servers, applies its ideological choices by saving energy resources thanks to
    innovative web hosting offers as well as to new developments and procedures. IKOULA has been able to choose to
    reduce its carbon footprint thanks to a low-consumption infrastructure of dedicated and virtual servers. The
    dedicated servers are hosted in France in a latest-generation Data Centre in tune with the environment.&quot; &quot;**Since
    1 November 2015, IKOULA has been using electricity from 100% renewable energy sources for its Data Centres.&quot; <a
            href="https://www.ikoula.com/en/web-environmental-hosting">(source)</a> &quot;Ikoula is the owner of its
    DataCenter 1750 m² in Reims that hosts more than 4,000 servers. Pioneer of green hosting, with over 4,000 enterprise
    customers and 25% annual growth, Ikoula places innovation, simplicity and customer satisfaction at the heart of its
    solutions. We are also present in other two DataCenters in France, Telecity Courbevoie (92) and TeleHouse 2 in
    Paris. The DataCenter in Reims outclasses the Parisian rooms.&quot; <a
            href="https://www.ikoula.com/en/ikoula-datacenter-france">(source)</a></p>
<p><strong>Many different data centers:</strong></p>
<p>a greenwashing cadidate?</p>
<p>&quot;Since 1 November 2015, IKOULA has been using electricity from 100% renewable energy sources for its Data
    Centres.&quot;</p>
<p>The <a href="https://extranet.ikoula.com/weathermap/">Ikoula Network Map</a> says that they using different data
    centers from: Dec-IX, AmsIX, Hurricane Networks, Scaleway, Equinix (100% green), Telia, OVH, Hetzner (<a
            href="https://framagit.org/greenfediverse/green-instances/-/blob/master/additional-info/hetzner.md">see our
        research</a>), gtt, FranceIX</p>
<p><em>About the Equinix data center:</em></p>
<p>The <a href="https://en.wikipedia.org/wiki/Equinix">Equinix</a> data center in Paris, Telecity Courbevoie (<a
        href="https://www.equinix.com/data-centers/europe-colocation/france-colocation/paris-data-centers/pa7">PA7
    Courbevoie Data Center</a>), or &quot;Paris PA7&quot; uses renewable energy with a 100% coverage &quot;from GP - <a
        href="https://en.wikipedia.org/wiki/Guarantee_of_origin">GoOs</a>&quot;. <a
        href="https://sustainability.equinix.com/map-of-initiatives/">(source)</a>. - published green energy certificate
    is not available.</p>
<p>Equinex in general is not 100% green, but trying to <a
        href="https://sustainability.equinix.com/sustainability-approach/">(source)</a>. Equinex has a &quot;long-term
    goal of using 100% clean and renewable energy&quot; <a
            href="https://www.equinix.com/data-centers/design/green-data-centers">(source)</a>. &quot;We&#39;ve set a
    goal to be climate neutral by 2030, backed by science-based targets, across our global portfolio and data center
    platform&quot; <a href="https://www.equinix.com/about/sustainability">(source)</a>. Equinex claim things that are
    obviously exaggerated, like: &quot;We are the most &#39;green awarded&#39; colocation company in the world&quot; <a
            href="https://www.equinix.com/resources/infopapers/green-data-centers">(source)</a>. &quot;In 2020, Equinix
    undertook a year of transition to create foundational elements that will be integral to achieving our long-term goal
    of 100% clean and renewable energy across our global interconnection and data center platform. As an RE100 member
    since 2015 we believe in the promise of a greener grid. We are committed to reaching our goal of 100% clean and
    renewable energy usage across our global portfolio by 2025 for existing sites and by 2030 for future sites and
    acquisitions. Through the EU Climate Neutral Data Center Operator Pact we also committed to reaching 75% renewable
    by 2025 and 100% renewable by 2030. Equinix is already 99% renewable throughout Europe as of 2020.&quot; <a
            href="https://sustainability.equinix.com/environment/scaling-renewable-energy/">(source)</a></p>
<p><em>About the energy of Equinix data centers:</em></p>
<p>&quot;We procure renewable energy in all three regions of the world in which we operate. Our renewable power comes
    from a variety of projects and products that are certified to meet rigorous reporting and retirement standards. Our
    renewable energy is sourced from: Wind farms in Oklahoma and Texas, renewable energy certificates and local clean
    energy or low-carbon programs in the U.S. and Brazil, Certified green power from our suppliers in Europe,
    International certificates from renewable energy projects in China, Japan and Vietnam. In alignment with our
    principles, we are actively considering opportunities to increase the quality, locality, and additionality of our
    renewable energy purchases. Key focus areas going forward include transitioning from certificates to VPPAs where
    feasible and cost-effective and adding to our coverage in Asia-Pacific.&quot; <a
            href="https://sustainability.equinix.com/environment/scaling-renewable-energy/">(source)</a></p>
<p><img src="./assets/Renewable-Energy-by-Region-3.png" alt="RenewableEnergybyRegion3.png"></p>
<p><a href="https://sustainability.equinix.com/environment/scaling-renewable-energy/">(source - last retrieved:
    01/2022)</a></p>
<p><a href="https://www.telehouse.net/paris-th2-connectivity-hub/">Telehouse 2</a> datacenter:</p>
<p>Telehouse claim to use renewable energy through tradable certificates by &quot;<a
        href="https://havenpower.com/01473725943">HavenPower</a>&quot; (HavenPower are now <a
        href="https://energy.drax.com/">Drax</a>), a british &quot;power generation business. The principal downstream
    enterprises are based in the UK and include Drax Power Limited, which runs a biomass and coal fueled power station&quot;
    <a href="https://en.wikipedia.org/wiki/Drax_Group">(source)</a>. There are lot of controversies (<a
            href="https://en.wikipedia.org/wiki/Drax_Group#Controversies">on wikipedia</a>).</p>
<p>Telehouse uploaded their certificate (.pdf) <a
        href="https://www.telehouse.net/wp-content/uploads/2021/09/Telehouse-International-Corporation-of-Europe_.pdf">here</a>.
</p>
<p><em>About the energy:</em></p>
<p><strong>Further research:</strong></p>
<p><a href="https://www.ikoula.com/en/ikoula-datacenter-france#tabs-1&quot;IKDC01&quot;">IKDC01 Datacenter</a> is a data
    center in <a href="https://en.wikipedia.org/wiki/Reims">Reims</a>, unclear which kind of energy - not transparent.
</p>
<p><a href="https://www.ikoula.com/en/ikoula-datacenter-france#tabs-2&quot;IKDC02&quot;">IKDC02 Datacenter</a> is a data
    center in <a href="https://en.wikipedia.org/wiki/Eppes,_Aisne">Eppes</a>, unclear which kind of energy - not
    transparent.</p>
<p>this is what they say: <a href="https://www.ikoula.com/en/web-environmental-hosting">https://www.ikoula.com/en/web-environmental-hosting</a>
</p>
<p><strong>Other notes:</strong></p>
<p>
    <a href="https://www.climateneutraldatacentre.net/signatories">https://www.climateneutraldatacentre.net/signatories</a>
</p>
