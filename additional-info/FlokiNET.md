<p><strong>Hosting Provider:</strong> <a href="https://flokinet.is">FlokiNET</a></p>
<p><strong>Published green energy certificate:</strong></p>
<p><strong>Critical green energy and sustainability notes:</strong></p>
<p><em>About the data center:</em></p>
<p style="text-align: justify">
    &quot;Situated up in the North Atlantic, Iceland occupies a unique position in the data center world. With abundant
    geothermal and hydroelectric
    energy sources, this rugged island is the site of the cleanest energy in the world. Iceland´s geographic location
    provides plenty of both water and wind used to generate energy, from either hydroelectric dams or, in super-heated
    form as steam in geothermal power plants. The second one, wind, provides a steady supply of natural cooling and
    helps to keep temperatures down. This temperate climate is the ideal for data centers. The energy is clean, and
    renewable. Pollution from energy production is non-existent and the carbon footprint is absolutely zero. Natural
    free cooling, amply provided by Kári (an Icelandic nick name for the wind), is used to control temperatures inside
    the data center, and helps to keep costs down. The average temperature at the data center location is 1.8°C in
    January and 10°C in July.&quot; <a href="https://flokinet.is/ecoresponsability.php">(source)</a></p>
<p><em>About the energy:</em></p>
<p style="text-align: justify">
    &quot;<a href="https://www.landsvirkjun.com/">Landsvirkjun</a> was founded on 1 July 1965 by the state of Iceland and
    the city of Reykjavik. It is the National Power Company of Iceland and
    operates <a href="https://www.landsvirkjun.com/powerstations">18 power stations</a> <a
            href="https://en.wikipedia.org/wiki/Landsvirkjun#Power_stations">(+wikipedia)</a> in Iceland concentrated on
    five main areas of operation <a href="https://en.wikipedia.org/wiki/Landsvirkjun">(source)</a>. &quot;<a
            href="https://en.wikipedia.org/wiki/Orkuveita_Reykjav%C3%ADkur">Orkuveita Reykjavíkur</a> (English:
    Reykjavík Energy) is an Icelandic energy and utility company that provides electricity, geothermal water for
    heating, and cold water for consumption and fire fighting.&quot;</p>
<p><strong>Further research:</strong></p>
<p><strong>Other notes:</strong></p>
